<?php
/**
 * @file
 * Example hook code for Ctools Double Modal Choice.
 */

/**
 * Alter the list of href mappings.
 *
 * Because there is no way to know whether the href of a Ctools modal link would
 * actually work as an actual browser tab, we are going to have to rely on other
 * modules to tell us what the mapping should be. Any Ctools modal links that
 * are not idenfified here will not be acted on.
 */
function hook_ctools_double_modal_choice_map_alter(&$map) {

  // The percent (%) character can be used, along with an arbitrary label, to
  // indicate variable components in the path that should be carried over from
  // the Ctools modal link's URL to the URL for opening a new tab.
  // Also note, the base path (eg, opening slash) should not be included.
  $map['my/ctools/plugin/%id/ajax/edit'] = 'some/other/url/edit/%id';

  // This would tell Ctools Double Modal Choice to change the href
  // "my/ctools/plugin/123/ajax/edit" into "some/other/url/edit/123" when it is
  // opening a new tab.
}
