<?php
/**
 * @file
 * Administrative page callbacks for the Ctools Double Modal Choice module.
 */

/**
 * General configuration form for Ctools Double Modal Choice behaviour.
 */
function ctools_double_modal_choice_admin_settings() {

  $form['ctools_double_modal_choice_auto_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically open new tabs'),
    '#default_value' => variable_get('ctools_double_modal_choice_auto_tab', 0),
    '#description' => t('Normally this module gives the user a *choice* (hence the name) of whether to replace the current modal or open the new one in a new tab. Check this box to remove that choice and just always open a new tab.'),
  );

  return system_settings_form($form);
}
