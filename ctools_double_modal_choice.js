/**
 * @file
 * Javascript code for Ctools Double Modal Choice.
 */

(function ($) {

  Drupal.behaviors.ctoolsDoubleModalChoice = {
    attach: function (context, settings) {

      // Prepare our map of href regular expressions.
      var hrefMap = {};
      $.each(Drupal.settings.ctools_double_modal_choice.map, function(source, destination) {

        // Since we're dealing with URLs, first add Drupal's base path.
        source = settings.basePath + source;
        destination = settings.basePath + destination;

        // In the settings, there will be hrefs like: foo/%bar/baz/%bob/fap
        // and we need to convert the % variables into regex capture groups,
        // but keep track of the names of the variables as a "sections" array.
        var sourceParts = source.split('/');
        var regexParts = [];
        var sections = [];
        $.each(sourceParts, function(index, sourcePart) {
          if (sourcePart.indexOf('%') == 0) {
            sections.push(sourcePart);
            regexParts.push('(.*)');
          }
          else {
            regexParts.push(sourcePart);
          }
        });
        hrefMap[source] = {
          sections: sections,
          regex: regexParts.join('\\/'),
          destination: destination
        };
      });

      $('.ctools-modal-content a.ctools-use-modal').once('ctools-double-modal-choice').each(function() {

        // Only act on Ctools link that we have advance knowledge about in our
        // href map. We don't know that ALL Ctools links will work in new tabs.
        var ctoolsHref = $(this).attr('href');
        var hrefInfo;
        // Crunch our regular expressions.
        $.each(hrefMap, function(source, info) {
          var regex = new RegExp(info.regex);
          var matches = ctoolsHref.match(regex);
          if (matches) {
            hrefInfo = $.extend(true, {}, info);
            hrefInfo.matches = matches;
            return false;
          }
        });

        // We did not find a match for this Ctools link, so ignore it.
        if (!hrefInfo) {
          return;
        }

        // Remove the cruft from the regex results, so we're only left with
        // the capture groups, if any.
        hrefInfo.matches.shift();
        delete(hrefInfo.matches.input);
        delete(hrefInfo.matches.index);

        // Finally update the destination with our regex capture groups.
        $.each(hrefInfo.sections, function(index, sectionName) {
          hrefInfo.destination = hrefInfo.destination.replace(sectionName, hrefInfo.matches[index]);
        });

        // The jQuery UI dialog definition.
        var dialogDef = {
          modal: true,
          dialogClass: 'ctools-double-modal-choice',
          width: 400,
          buttons: [
            {
              text: Drupal.t('Open the popup in a new tab'),
              click: function() {
                window.open(hrefInfo.destination, '_blank');
                $(this).dialog("close");
              }
            },
            {
              text: Drupal.t('Open the popup here (this popup will be closed)'),
              click: function() {
                original.click();
                $(this).dialog("close");
              }
            }
          ]
        };

        var original = this;

        // Create a copy of the ctools link, but without Ctools modal behavior.
        var imposter = $(this)
          .clone()
          .removeClass('ctools-use-modal')
          .click(function(e) {
            // Make sure nothing else happens.
            e.preventDefault();
            // If we should always open new tabs, just do that.
            if (Drupal.settings.ctools_double_modal_choice.tab) {
              window.open(hrefInfo.destination, '_blank');
            }
            // Otherwise give the user a choice in a dialog.
            else {
              $('<p>')
                .append(Drupal.t('You are about to open a new popup. Please choose an option.'))
                .dialog(dialogDef);
            }
          });

        // Put the clone just after the original, and hide the original.
        $(this).after(imposter).hide();
      });

    }
  };
})(jQuery);
